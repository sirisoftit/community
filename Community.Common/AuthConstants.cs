﻿namespace Community.Common
{
    public static class AuthConstants
    {
        public const string DelegatedIdentityHeaderName = "DELEGATEDIDENTITY";
    }
}
