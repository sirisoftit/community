﻿namespace Community.Common
{
    public static class ApplicationConstants
    {
        public static string APP_NAME = "Community";
        public static string APP_PREFIX = "co";
    }
}
