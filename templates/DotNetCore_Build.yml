parameters:
# build
- name: projectsToBuild
  type: string
  default: '**/*.csproj'
- name: preBuildSteps
  type: stepList
  default: []
- name: restoreArguments
  type: string
  default: ''
- name: buildArguments
  type: string
  default: ''
- name: postBuildSteps
  type: stepList
  default: []
# nuget
- name: pack
  type: boolean
  default: true
- name: projectsToPack
  type: string
  default: '**/*.Common.csproj'
- name: majorVersion
  type: string
  default: 1
- name: minorVersion
  type: string
  default: 0
# test
- name: test
  type: boolean
  default: true
- name: publishTests
  type: boolean
  default: true
- name: projectsToTest
  type: string
  default: '**/*.*Tests.csproj'
- name: testArguments
  type: string
  default: ''
- name: jobName
  type: string
  default: Build

jobs:
- job: ${{parameters.jobName}}
  workspace:
    clean: all
  pool:
    vmImage: ubuntu-latest
 # pool:  # This is what associa uses.
 #   name: Default
 #   demands:
 #     - Agent.OS -equals Linux
  variables:
    NugetVersion: ${{parameters.majorVersion}}.${{parameters.minorVersion}}.$(Build.buildNumber)
  steps:
# Build
  - task: UseDotNet@2
    inputs:
      packageType: sdk
      version: 2.1.x
  - task: UseDotNet@2
    inputs:
      packageType: sdk
      version: 3.x
  - task: UseDotNet@2
    inputs:
      packageType: sdk
      version: 6.x
  - ${{parameters.preBuildSteps}}
  - task: NuGetAuthenticate@0
  - task: DotNetCoreCLI@2
    displayName: dotnet restore ${{parameters.projectsToBuild}}
    inputs:
      command: custom
      custom: restore
      projects: ${{parameters.projectsToBuild}}
      arguments: --source https://api.nuget.org/v3/index.json --source https://pkgs.dev.azure.com/associa/_packaging/ATS/nuget/v3/index.json ${{parameters.restoreArguments}}
  - task: DotNetCoreCLI@2
    displayName: dotnet build ${{parameters.projectsToBuild}}
    inputs:
      command: build
      projects: ${{parameters.projectsToBuild}}
      arguments: --configuration Release --no-restore ${{parameters.buildArguments}}
  - ${{parameters.postBuildSteps}}
# Test
  - ${{if eq(parameters.test, true)}}:
    - task: DotNetCoreCLI@2
      displayName: Add coverlet.msbuild to ${{parameters.projectsToTest}}
      inputs:
        command: custom
        custom: add
        projects: ${{parameters.projectsToTest}}
        arguments: package coverlet.msbuild
    - task: DotNetCoreCLI@2
      displayName: dotnet test ${{parameters.projectsToTest}}
      inputs:
        command: test
        projects: ${{parameters.projectsToTest}}
        arguments: /p:CollectCoverage=true /p:CoverletOutputFormat=cobertura ${{parameters.testArguments}}
    - task: DotNetCoreCLI@2
      displayName: Install ReportGenerator tool  
      inputs:
        command: custom
        custom: tool
        arguments: install --tool-path . dotnet-reportgenerator-globaltool
    - script: ./reportgenerator -reports:$(Build.SourcesDirectory)/**/coverage*.cobertura.xml -targetdir:$(Build.SourcesDirectory)/coverlet/reports -reporttypes:"Cobertura" -assemblyfilters:"-*Tests;-*Migrations"
      displayName: Merge coverage reports with ReportGenerator
    - task: PublishCodeCoverageResults@1
      displayName: Publish code coverage
      inputs:
        codeCoverageTool: Cobertura
        summaryFileLocation: $(Build.SourcesDirectory)/coverlet/reports/Cobertura.xml
    - ${{if eq(parameters.publishTests, true)}}:
      - task: DotNetCoreCLI@2
        displayName: Publish Tests ${{parameters.projectsToTest}}
        inputs:
          command: publish
          publishWebProjects: false
          projects: ${{parameters.projectsToTest}}
          arguments: --output $(build.artifactstagingdirectory)/tests --no-build
          zipAfterPublish: false
      - task: PublishPipelineArtifact@1
        displayName: Save Tests Artifact
        condition: succeededOrFailed()
        inputs:
          targetPath: $(build.artifactstagingdirectory)/tests
          artifactName: tests
# Publish common packages to Nuget
  - ${{if eq(parameters.pack, true)}}:
    - task: DotNetCoreCLI@2
      displayName: Add Microsoft.SourceLink.Bitbucket.Git to ${{parameters.projectsToPack}}
      inputs:
        command: custom
        custom: add
        projects: ${{parameters.projectsToPack}}
        arguments: package Microsoft.SourceLink.Bitbucket.Git
    - task: DotNetCoreCLI@2
      displayName: dotnet pack ${{parameters.projectsToPack}}
      inputs:
        command: pack
        includesymbols: true
        includesource: true
        nobuild: true
        configuration: Release
        packagesToPack: ${{parameters.projectsToPack}}
        packDirectory: $(Build.ArtifactStagingDirectory)/nuget
        versioningScheme: byEnvVar
        versionEnvVar: NugetVersion
    - task: DotNetCoreCLI@2
      displayName: dotnet push to nuget
      condition: and(succeeded(), eq(variables['Build.SourceBranch'], 'refs/heads/master'))
      inputs:
        command: push
        searchPatternPush: $(Build.ArtifactStagingDirectory)/nuget/*.symbols.nupkg;!$(Build.ArtifactStagingDirectory)/nuget/*.Tests.nupkg
        feedPublish: ATS
    - task: PublishPipelineArtifact@1
      displayName: Save Nuget Package Artifact
      condition: succeededOrFailed()
      inputs:
        targetPath: $(build.artifactstagingdirectory)/nuget
        artifactName: nuget
