using Amazon.CDK;
using Amazon.CDK.AWS.EC2;
using Constructs;

namespace CommunityCdk
{
    public class CommunityCdkStack : Stack
    {
        internal CommunityCdkStack(Construct scope, string id, IStackProps props = null) : base(scope, id, props)
        {
            // The code that defines your stack goes here

            // The code that defines your stack goes here


            NatInstanceProvider natGatewayProvider = NatProvider.Instance(new NatInstanceProps
            {
                InstanceType = new InstanceType("t3.nano")
            });

            // Create a new vpc. 
            var mainVpc = new Vpc(this, "MainVpcComm1", new VpcProps
            {
                Cidr = "10.0.0.0/16",
                MaxAzs = 3,
                SubnetConfiguration = new ISubnetConfiguration[]
                 {
                    new SubnetConfiguration
                    {
                        CidrMask = 24,
                        SubnetType = SubnetType.PUBLIC,
                        Name = "PublicSubnet"
                    },
                    new SubnetConfiguration
                    {
                        CidrMask = 24,
                        SubnetType = SubnetType.PRIVATE_WITH_NAT,
                        Name = "PrivateSubnet"
                    },
                    new SubnetConfiguration
                    {
                        CidrMask = 28,
                        SubnetType = SubnetType.PRIVATE_ISOLATED,
                        Name = "IsolatedSubnet"
                    }
                 },
                NatGatewayProvider = natGatewayProvider
            });

            Aspects.Of(mainVpc).Add(new Tag("Name", "MainVPC"));

            AddName(mainVpc.PublicSubnets, "Name", "PublicSubnet");
            AddName(mainVpc.PrivateSubnets, "Name", "PrivateSubnet");
            AddName(mainVpc.IsolatedSubnets, "Name", "IsolatedSubnet");

        }

        private void AddName(ISubnet[] subnets, string tagName, string tagValue)
        {
            int i = 1;
            foreach (var subnet in subnets)
            {
                Aspects.Of(subnet).Add(new Tag(tagName, tagValue + i++.ToString()));
            }
        }
    }
}
